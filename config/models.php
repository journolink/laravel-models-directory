<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Models Namespace
    |--------------------------------------------------------------------------
    |
    | Here you may specify the namespace for Model classes, relative to the
    | root application namespace, for instance to have Model classes reside
    | inside app/Models, set this value to "Models"
    |
    */

    'namespace' => env('APP_MODELS_NAMESPACE', ''),

];
