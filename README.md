# Laravel Models Directory

Customise your default "models" directory within your Laravel application

**Note:** Currently tested and working against Laravel 5.8 installs

## Install

```bash
composer require journolink/laravel-models-dir
```

Auto-discovery is enabled. If discovery does not run automatically, run:

```bash
php artisan package:discover
```

## Configuration

**Note:** By default, this package will use the `app` directory, as is the Laravel default.

The namespace (relative to the application root namespace) for model classes can be specified using the following variable in the relevant `.env` file. 

With a root namespace of `App`, the following configuration will place model classes in the `App\Models` namespace.
```dotenv
APP_MODELS_NAMESPACE=Models
```

Alteratively, this package publishes a configuration file to store the model classes namespace:
```bash
php artisan vendor:publish --provider=JournoLink\LaravelModelsDir\ServiceProvider
```

## Usage

This package extends the built-in `make:model` to place model classes in the correct directory, including the use of flags to create Factories, Migrations etc.

Additionally, this package provides a command to migrate existing models into the correct directory, as well as rewriting existing references to models:
```bash
php artisan models:namespace
```

The command has the following options available:

```bash
Arguments:
  namespace        The new models namespace, relative to the application root. Will also update config (optional)

Options:
  --from[=DIR]     The current models namespace, relative to the application root namespace [default: "/"]
  --depth[=DEPTH]  The search depth for models in the existing models directory [default: "0"]
```