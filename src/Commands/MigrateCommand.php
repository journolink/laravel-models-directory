<?php

namespace JournoLink\LaravelModelsDir\Commands;

use ReflectionClass;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\Finder;
use Illuminate\Database\Eloquent\Model;

class MigrateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'models:namespace  {namespace? : The new models namespace, relative to the app_path. Will also update config}
                                              {--from=/ : The current models namespace, relative to the app_path namespace}
                                              {--depth=0 : The search depth for models in the current directory}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move models into the configured Models directory';

    /**
     * The namespace for models, as provided by config
     *
     * @var string
     */
    protected $namespace;

    /**
     * The current directory containing the models
     *
     * @var string
     */
    protected $currentDirectory;

    /**
     * The target directory for Model classes
     *
     * @var string
     */
    protected $newDirectory;

    /**
     * The current application namespace
     *
     * @var string
     */
    protected $currentRoot;

    /**
     * The target namespace for Model classes
     *
     * @var string
     */
    protected $newRoot;

    /**
     * @var array
     */
    protected $models = [];


    /**
     * Execute the console command.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function handle()
    {
        if ($this->argument('namespace')) {
            $this->updateConfig();
        }

        $this->namespace = trim(config('models.namespace'), '/');
        $this->currentRoot = trim($this->laravel->getNamespace(), '\\');
        $this->newRoot = $this->currentRoot .'\\'. $this->namespace;
        $this->currentDirectory = app_path(trim($this->option('from'), '/'));
        $this->newDirectory = app_path($this->namespace);

        $this->migrateModels();
        $this->migrateReferences();

        $this->line('Finished!');
    }

    /**
     * Publish, if necessary, and update the configuration file
     *
     * @return void
     */
    protected function updateConfig()
    {
        if ($this->confirm('Do you want to update the configuration file on disk?')) {
            // If the config file hasn't been published yet, we need to publish it here to update it
            $this->publishConfig();

            $newContents = preg_replace(
                '/^([^\n]*\'namespace\' => )(.*),\s*$/m',
                "$1'". $this->argument('namespace') ."',",
                File::get(config_path('models.php'))
            );

            File::put(config_path('models.php'), $newContents);
            $this->info('Updated configuration value');
        }

        $this->laravel['config']->set('models.namespace', $this->argument('namespace'));
    }

    /**
     * Migrate the model files into the new directory with the correct namespace
     *
     * @return void
     * @throws \ReflectionException
     */
    protected function migrateModels()
    {
        $this->line('Migrating models into '. $this->newDirectory);

        $files = Finder::create()
            ->in($this->currentDirectory)
            ->depth($this->option('depth'))
            ->name('*.php');

        /** @var \SplFileInfo $file */
        foreach ($files as $file) {
            $namespace = $this->getFileNamespace($file->getRealPath());

            if (!$namespace || !$this->isModelClass($namespace['fullNamespace'])) {
                continue;
            }

            $newFile = rtrim($this->newDirectory, '/') .'/'. $file->getFilename();
            $className = str_replace('.php', '', $file->getFilename());

            $this->models[$namespace['fullNamespace']] = $this->newRoot .'\\'. $className;

            $this->replaceIn(
                $file->getRealPath(),
                'namespace '. $namespace['namespace'] .';',
                'namespace '. $this->newRoot .';'
            );

            $this->makeDirectory($newFile);

            File::move(
                $file->getRealPath(),
                $newFile
            );

            $this->line('Migrated '. $className);
        }

        if (count($this->models) === 0) {
            $this->line('Nothing to migrate');
        }
    }

    protected function migrateReferences()
    {
        $this->line('Replacing references in files....');

        $files = Finder::create()
            ->in(base_path())
            ->exclude('vendor')
            ->exclude('storage')
            ->exclude('public')
            ->exclude('resources')
            ->exclude('node_modules')
            ->name('*.php');

        /** @var \SplFileInfo $file */
        foreach ($files as $file) {
            foreach ($this->models as $model => $replace) {
                $this->replaceIn(
                    $file->getRealPath(),
                    $model,
                    $replace
                );
            }
        }
    }

    /**
     * Replace the given string in the given file.
     *
     * @param  string  $path
     * @param  string|array  $search
     * @param  string|array  $replace
     * @return void
     */
    protected function replaceIn($path, $search, $replace)
    {
        if (File::exists($path)) {
            File::put($path, str_replace($search, $replace, File::get($path)));
        }
    }

    /**
     * Retrieve the root parent class for the given class
     *
     * @param $namespace
     * @return \ReflectionClass
     * @throws \ReflectionException
     */
    protected function getRootClass($namespace)
    {
        $reflector = new ReflectionClass($namespace);

        $parent = $reflector->getParentClass();

        while ($parent->getParentClass() instanceof ReflectionClass) {
            $parent = $parent->getParentClass();
        }

        return $parent;
    }

    /**
     * @param $namespace
     * @return bool
     * @throws \ReflectionException
     */
    protected function isModelClass($namespace)
    {
        return $this->getRootClass($namespace)->getName() === Model::class;
    }

    /**
     * @param $path
     * @return array|null
     */
    protected function getFileNamespace($path)
    {
        preg_match('/^namespace (.*);$/m', File::get($path), $matches);

        if ($namespace = isset($matches[1]) ? $matches[1] : null) {
            return [
                'namespace' => $namespace,
                'fullNamespace' => $namespace .'\\'. str_replace('.php', '', basename($path)),
            ];
        }

        return null;
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string  $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (! File::isDirectory(dirname($path))) {
            File::makeDirectory(dirname($path), 0777, true, true);
            $this->line('Created directory '. dirname($path));
        }

        return $path;
    }

    /**
     * Publishes the package configuration, if required
     *
     * @return void
     */
    protected function publishConfig()
    {
        if (!File::exists(config_path('models.php'))) {
            File::copy(__DIR__ . '/../../config/models.php', config_path('models.php'));
            $this->info('Published '. config_path('models.php'));
        }
    }
}
