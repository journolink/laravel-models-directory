<?php

namespace JournoLink\LaravelModelsDir;

use Illuminate\Filesystem\Filesystem;
use JournoLink\LaravelModelsDir\Commands\MigrateCommand;
use JournoLink\LaravelModelsDir\Commands\ModelMakeCommand;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/models.php',
            'models'
        );
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/models.php' => config_path('models.php'),
        ]);

        $this->commands([
            MigrateCommand::class,
        ]);

        $this->app->extend(
            'command.model.make',
            function ($concrete, $app) {
                return new ModelMakeCommand(
                    $app[Filesystem::class]
                );
            }
        );
    }
}
